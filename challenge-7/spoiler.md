# spoiler 

```
docker run --rm \
    --volume $(pwd):/src:z \
    --volume "$DATA_DIRECTORY":/usr/share/dependency-check/data:z \
    --volume $(pwd)/odc-reports:/report:z \
    owasp/dependency-check \
    --scan /src \
    --format "ALL" \
    --enableExperimental \
    --project test \
    --out /report
```